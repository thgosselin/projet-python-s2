from random import *

######### SECTION : GESTION DES CARTES #################
########################################################

#Cette fonction donne le paquet initial détenu par chaque joueur
def paquet_initial():
    # ce paquet est initialement prévu pour une partie à 5 lignes,
    # des segments de 20 cases, un start = 4 et terminus = 10
    return [2] * 3 + [3] * 3 + [4] * 3 + [5] * 3 + [6] * 3 + [9] * 3

#Il va falloir stocker tout au long de la partie le paquet de cartes de chaque joueur
#Pour cela, on va manipuler un dictionnaire qui admettra les noms des joueurs
#comme clés et leur paquet comme valeur.
#Au début de la partie, les joueurs auront des paquets équivalents, mais ceux-ci évolueront
#au fur et à mesure de la partie, en fonction des déplacements de chacun.
#Cette fonction prend en entrée la liste des joueurs et renvoie le dictionnaire.
def dico_paquets_initial(liste_coureurs):
    dico = {}
    paquet = paquet_initial() #on initialise le paquet avec la fonction paquet_initial
    for coureur in liste_coureurs: #pour les coureur dans liste_coureurs
        dico[coureur] = paquet.copy() #ajout copy d'un paquet initial au joueur dans le dico
    return dico

#Chaque joueur possède aussi une défausse contenant les cartes déplacements
#qu'il n'a pas choisi. Lorsque la défausse sera vide, on la mélange et remet son contenu
#dans le paquet. Au début de la partie, la défausse est vide.
#Cette fonction crée donc un dictionnaire où les clés sont les joueurs et les valeurs
#sont les défausses initiales (à savoir des listes vides)
def dico_defausses_initial(liste_joueurs):
    dico = {}
    for joueur in liste_joueurs:
        dico[joueur] = [] 
    return dico


def configurations_joueurs(liste_coureurs):
    dico_configurations = {} #création du dico
    for joueur in liste_coureurs: #pour les joueurs dans la liste_joueur
        while True: #demande de sasir pour le premier joueur
            choix = input("Quel mode pour le joueur " + joueur +" : Aleatoire ou Manuel ? (A/M) ").upper() #on demande le choix
            if choix == "A" or choix == "M": #si choix = A ou M
                break 
            else:
                print("Recommencez! ", end="")
        dico_configurations[joueur] = choix #on assigne le joueur avec le choix
    print("\nChoix des configurations effectué !\n")
    return dico_configurations

def choix_carte_alea(main_joueur):
    carte_choisie = choice(main_joueur) #choix d'une carte aléatoirement
    return carte_choisie

def choix_carte_manuel(nom_joueur, main_joueur):
    le_choix=True #initialise le choix a True
    while True: #tant qu'il n'y as pas de return la boucle tourne c'est a dire lorsqu'il y a une carte dans la main du joueur qui est un choix
        choix = input(f"Quel déplacement pour {nom_joueur} souhaitez-vous effectuer ? {main_joueur} ")
        if int(choix) in main_joueur: #si le choix est dans la main_joueur
            return choix #on affiche le choix
        else:
            print("Recommencez !")

def pioche_main_joueur(nom_joueur, dico_paquets, dico_defausses):
    if len(dico_paquets[nom_joueur]) >= 4: #vérifier si le paquet du joueur contient au moins 4 cartes
        main_joueur = dico_paquets[nom_joueur][:4] #si oui, prendre les 4 premières cartes pour former la main du joueur
        dico_paquets[nom_joueur] = dico_paquets[nom_joueur][4:] #mettre à jour le paquet du joueur en retirant les cartes de sa main
    else: #si le paquet du joueur contient moins de 4 cartes
        main_joueur = dico_paquets[nom_joueur] 
        dico_paquets[nom_joueur] = [] #vide le paquet du joueur
        dico_paquets[nom_joueur].extend(dico_defausses[nom_joueur]) #ajout carte et defausse dans la main du joueur 
        shuffle(dico_paquets[nom_joueur]) #mélanger le paquet du joueur (defausse incluse)
        dico_defausses[nom_joueur] = [] #vide le paquet defausse a vide
        main_joueur.extend(dico_paquets[nom_joueur][:4 - len(main_joueur)]) #extend(ajoute les éléments d'une liste (ou d'un autre objet itérable) à la fin de la liste sur laquelle elle est appelée)
        dico_paquets[nom_joueur] = dico_paquets[nom_joueur][4 - len(main_joueur):]
    return main_joueur

def selection_carte_main(nom_joueur, main_joueur, choix, dico_defausses):
    main_joueur_sans_choix = [] #initialise min_joueur_sans_choix
    for carte in main_joueur: #si la carte est dans la main du joueur
        if carte != choix: #et qu'elle n'est pas differente au choix
            main_joueur_sans_choix.append(carte) #alors on l'ajoute a main_joueur_sans_choix
    dico_defausses[nom_joueur] += main_joueur_sans_choix #on ajoute dans la deffause du joueur main_joueur_sans_choix

def choix_deplacement(nom_joueur, dico_paquets, dico_defausses, configurations, affichage=False):
    main_joueur = pioche_main_joueur(nom_joueur, dico_paquets, dico_defausses) #piocher la main du joueur
    config_joueur = configurations.get(nom_joueur, None) #récupérer la configuration du joueur
    # Sélectionner une carte de déplacement
    if config_joueur == "A":  # Automatique
        carte_selectionnee = choix_carte_alea(main_joueur)
    if config_joueur == "M":  # Manuelle
        carte_selectionnee = choix_carte_manuel(nom_joueur, main_joueur)
    selection_carte_main(nom_joueur, main_joueur, int(carte_selectionnee), dico_defausses) #mettre à jour la défausse après sélection de la carte
    dico_defausses[nom_joueur].append(int(carte_selectionnee))  #ajouter la carte sélectionnée à la défausse du joueur
    if affichage:  # afichage du contenu du paquet et de la défausse pour ce joueur, si demandé
        print(f"Le paquet du joueur {nom_joueur} contient : {dico_paquets[nom_joueur]}") 
        print(f"La defausse du joueur {nom_joueur} contient : {dico_defausses[nom_joueur]}")
    return carte_selectionnee





