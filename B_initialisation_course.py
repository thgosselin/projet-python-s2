from random import *

######### SECTION : INITIALISATION DE LA COURSE ##########
##########################################################


# A COMPLETER

def numero_de_case(nom_joueur,dico_positions):
    return int(dico_positions[nom_joueur][1:])

def creer_liste_joueurs(nb_joueurs):
    liste = [] #on créé une liste
    cpt = 0 #initialisation d'un compteur
    while cpt != nb_joueurs: #tant que le compteur n'est pas egal au nb_joueurs
        joueur="J" + str(cpt) #on ajoute un joueur J + cpt
        liste.append(joueur) #on l'ajoute a la liste
        cpt=cpt+1 #incrémentation du compteur
    return liste


def creer_listes_positions(nb_lignes, taille_seg):
    nb_cases_total = nb_lignes * taille_seg
    liste_vide = ["__"] * nb_cases_total #on fait une liste avec les emplacement vide * nb_lignes * taille_seg
    return liste_vide, liste_vide

def placer_joueur_alea(position_dispos):
    choix = choice(position_dispos) #on fait un choix
    position_dispos.remove(choix) #puis on l'enleve de la liste position_dispos
    return choix



def initialiser_jeu_alea(liste_joueurs, positions_droite, positions_gauche, start):
    for joueur in liste_joueurs: # pour les joueurs dans la liste de joueurs
        x = randint(0,1)
        if x == 1 :
            pos = placer_joueur_alea(positions_droite[0:start]) #on place le joueur a un emplacement
            positions_droite[positions_droite.index(pos)] = joueur #on remplace la position du joueur dans la liste des positions droite par le nom du joueur
        elif x == 0:
            pos = placer_joueur_alea(positions_gauche[0:start]) #on place le joueur a un emplacement
            positions_gauche[positions_gauche.index(pos)] = joueur #on remplace la position du joueur dans la liste des positions gauche par le nom du joueur




def construit_dico_positions(positions_droite, positions_gauche, start):
    dico_positions = {} #on creer le dico positions
    for i in range(len(positions_droite)):
        if positions_droite[i] != "__": #si la positions coté droit n'est pas vide
            dico_positions[positions_droite[i]] = "D" + str(i) #ajout de la positon du joueur dans le dico avec D
        if positions_gauche[i] != "__": #si la positions coté gauche n'est pas vide
            dico_positions[positions_gauche[i]] = "G" + str(i) #ajout de la positon du joueur dans le dico avec G
        if i >= start:
            break
    return dico_positions
