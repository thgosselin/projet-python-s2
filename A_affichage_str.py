
######### SECTION : AFFICHAGE DU JEU ##########
###############################################

#Fonction ligne_vers_droite
#Pour représenter une ligne de route, où les coureurs se déplacent vers la droite
#positions est une liste contenant des caractères à afficher

def ligne_vers_droite(positions):
    L = "> " + positions[0] #chaine de caractère avec > et la première lettre ou symbole
    cpt = 1 #initialisation d'un compteur a 1 car deja mis le première lettre ou symbole
    while cpt != len(positions): #boucle while pour faire toute les lettres ou symbole
        L = L + " | "+ positions[cpt] #ajout de chaque éléments et du symbole |
        cpt = cpt+1 #incrémentation du compteur
    L=L+" | " #ajout du dernier |
    return L



#Fonction route_vers_droite
#Une route est constituée de deux lignes en parallèle, pour représenter les deux voies.
#Chaque case est constituée de deux côtés, droite et gauche.
#positions_droite et positions_gauche sont des listes de même longueur contenant des caractères
def route_vers_droite(positions_droite, positions_gauche):
    route="" #initialisation de la chaine route
    route=route+ligne_vers_droite(positions_gauche)+"\n"+ligne_vers_droite(positions_droite) #on ajoute la ligne gauche puis droite
    return route

#Fonction inverse
#On souhaite maintenant créer des routes, où les coureurs se dirigent vers la gauche
#Cette fonction inverse une ligne, en enlevant le dernier élément et le remplaçant par "<"
def inverse(ligne):
    return ligne[:0:-1] + "<"


#Fonction route_vers_gauche
#Cette fonction renvoie une route où les coureurs se dirigent vers la gauche.
def route_vers_gauche(positions_droite, positions_gauche):
    route="" #initialisation de la chaine route
    route=route+inverse(ligne_vers_droite(positions_droite))+"\n"+inverse(ligne_vers_droite(positions_gauche)) #on ajoute la ligne gauche puis droite en l'inversant
    return route


#Fonction ligne_depart
#Les routes de départ sont forcément vers la droite par défaut.
#Elles commencent par ">>>>>" au lieu de ">"
#Une zone de start est présente au début et termine avec un "X" au lieu de "|"
def ligne_depart(positions, start=0):
    if start == 0: #si le joueur commence à 0
        L = ">>>>> X " + positions[0] #oncréé la ligne avec le joueur en position 0
    else:
        L = ">>>>> " + positions[0] #sinon on créé la ligne
    cpt = 1 #Initialisation du compteur à 1
    while cpt != len(positions): #boucle while pour faire toute les lettres ou symbole |
        if cpt == start: #si le compteur arrive au départ du joueur
            L = L + " X " + positions[cpt] #il place le joueur à la place de |
            cpt = cpt + 1 #incrémentation du compteur
        L = L + " | " + positions[cpt] #ajout de chaque éléments et sylbole |
        cpt = cpt + 1 #incrémentatio du compteur
    L = L + " | " #ajout du dernier |
    return L



#Fonction route_depart
#Cette fonction représente une route de départ avec deux lignes de départ en parallèle
def route_depart(positions_droite, positions_gauche, start):
    route="" #initialisation de la route
    route=route+ligne_depart(positions_gauche,start)+"\n"+ligne_depart(positions_droite,start) #ajout de la ligne de depart sur ligne gauche et droite
    return route


#Fonction ligne_arrivee_vers_droite
#On représente désormais une ligne d'arrivée où les coureurs se dirigent vers la droite
#Un entier terminus > 1 indique l'emplacement de la ligne d'arrivee pour gagner la course
def ligne_arrivee_vers_droite(positions, terminus):
    L = "> " + positions[0] #chaine de caractère avec > et la première lettre ou symbole
    cpt = 1 #initialisation d'un compteur a 1 car deja mis le première lettre ou symbole
    while cpt != len(positions): #boucle while pour faire toute les lettres ou symbole
        if cpt == terminus: #si le compteur arrive a la ligne d'arrivee
            L = L + "| XXXX " #alors on créer la ligne d'arrivee
        L = L + " | "+ positions[cpt] #ajout de chaque éléments et du symbole |
        cpt = cpt+1 #incrémentation du compteur
    L=L+" | " #ajout du dernier |
    return L
    


#Fonction route_arrivee_vers_droite
##Cette fonction représente une route de départ avec
#deux lignes d'arrivee en parallèle
def route_arrivee_vers_droite(positions_droite, positions_gauche,terminus):
    route=""
    route=route+ligne_arrivee_vers_droite(positions_gauche,terminus)+"\n"+ligne_arrivee_vers_droite(positions_droite,terminus)
    return route


#Fonction route_arrivee_vers_gauche
#Même chose que route_arrivee_vers_droite sauf que cette fois_ci
#les coureurs se déplacent vers la gauche
def route_arrivee_vers_gauche(positions_droite, positions_gauche, terminus):
    route=""
    route=route+inverse(ligne_arrivee_vers_droite(positions_droite,terminus))+"\n"+inverse(ligne_arrivee_vers_droite(positions_gauche,terminus))
    return route


#Fonction affichage_route
#Etant donné un nombre de lignes, la longueur de chaque ligne (taille_seg),
#deux liste de positions (pour la droite et la gauche),
#on affiche la route avec les caractéristiques indiquées et le contenu des listes
#de positions. L'emplacement du départ sur la première ligne (start) et de l'arrivée
#sur la dernière (terminus) sont donnés comme arguments optionnels
#On suppose nb_lignes >= 2
def affichage_route(nb_lignes, taille_seg, positions_droite, positions_gauche, start=4, terminus=10):
    route=route_depart(positions_droite[0:taille_seg], positions_gauche[0:taille_seg],start)+"\n\n" #initialisation de la route avec le depart
    positions_droite_reste=positions_droite[taille_seg:]
    positions_gauche_reste=positions_gauche[taille_seg:]
    if nb_lignes>2:
        i=0
        while(i<nb_lignes-2):
            if i%2==0:
                route+=route_vers_gauche(positions_droite_reste[0:taille_seg],positions_gauche_reste[0:taille_seg]) +"\n\n" #alors on ajoute une ligne vers la gauche 
            if i%2==1:
                route+=route_vers_droite(positions_droite_reste[0:taille_seg],positions_gauche_reste[0:taille_seg])+"\n\n" #sinon vers la droite
            positions_droite_reste=positions_droite_reste[taille_seg:]
            positions_gauche_reste=positions_gauche_reste[taille_seg:]
            i+=1
        if i%2==1:
            route+=route_arrivee_vers_gauche(positions_droite_reste[0:taille_seg],positions_gauche_reste[0:taille_seg],terminus)+"\n\n" #alors on ajoute l'arrive
    return route

