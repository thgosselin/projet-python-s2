from D_deplacement_coureurs import *

######### SECTION : PHASE TERMINALE DU JEU #################
##############################################################

#Fonction distribution_fatigue

def distribution_fatigue(dico_positions, positions_droite, dico_defausses):
    for joueur, position in dico_positions.items(): #Parcours de chaque joueur et sa position dans le dictionnaire des positions
        indice_case = numero_de_case(joueur, dico_positions) #Obtention de l'indice de la case où se trouve le joueur
        if indice_case + 1 < len(positions_droite) and positions_droite[indice_case + 1] == "__": #Vérification si la case suivante est inoccupée et si elle existe dans positions_droite
            dico_defausses[joueur].append(2) #Si la case suivante est inoccupée, ajoute une carte Fatigue de valeur 2 à la défausse du joueur


#Cette fonction détermine les différents groupes de coureurs,
#qui sont séparés au moins par une case complètement vide
#Elle renvoie donc une liste de listes, dont le premier élément
#est le groupe étant le plus proche de la ligne de départ
# (donc le plus en retard dans la course)
#Dans chaque groupe, les coureurs sont aussi classés du plus en retard
#au plus proche de la ligne d'arrivée


def groupes_coureurs(dico_positions, positions_droite, positions_gauche):
    # Trouver le coureur le plus à gauche et le plus à droite
    j_min = le_moins_eloigne(dico_positions, positions_droite)
    j_max = le_plus_eloigne(dico_positions)
    # Trouver le numéro de case correspondant à ces coureurs
    case_min = numero_de_case(j_min, dico_positions)
    case_max = numero_de_case(j_max, dico_positions)
    x = case_min # Initialiser la variable x à la case la plus à gauche
    l_groupes = [] # Liste pour stocker les groupes de coureurs
    while x <= case_max: # Boucle pour parcourir les cases de la course
        # Initialiser un groupe pour cette case
        groupe = []
        # Récupérer les coureurs à droite et à gauche de la case x
        droite_x = positions_droite[x]
        gauche_x = positions_gauche[x]
        while droite_x != "__": # Tant qu'il y a un coureur à droite de la case x
            if gauche_x != "__": # Si un coureur est également présent à gauche, l'ajouter au groupe
                groupe.append(gauche_x)
            groupe.append(droite_x) # Ajouter le coureur à droite au groupe
            x += 1 # Passer à la case suivante
            # Mettre à jour les positions des coureurs à droite et à gauche
            droite_x = positions_droite[x]
            gauche_x = positions_gauche[x]
        if len(groupe) > 0: # Si le groupe contient des coureurs, l'ajouter à la liste des groupes
            l_groupes.append(groupe)
        x += 1 # Passer à la case suivante
    return l_groupes # Retourner la liste des groupes de coureurs


def decale_coureurs(groupe, dico_positions, positions_droite, positions_gauche):
    for nom_joueur in groupe: #Parcours du groupe de coureurs
        position = dico_positions[nom_joueur]  # Position actuelle du coureur
        avancee_coureurs(nom_joueur, dico_positions, positions_droite, positions_gauche, 1) #Décalage d'une case vers la ligne d'arrivée

def aspiration(dico_positions, positions_droite, positions_gauche):
    longBoucle = len(groupes_coureurs(dico_positions, positions_droite, positions_gauche))
    listGroup = groupes_coureurs(dico_positions, positions_droite, positions_gauche)
    for i in range(longBoucle):
        for groupe in listGroup[0:len(listGroup)-1]:
            joueurCase = numero_de_case(groupe[-1], dico_positions)
            if positions_droite[joueurCase + 1] == "__" and positions_droite[joueurCase + 2] != "__":
                decale_coureurs(groupe, dico_positions, positions_droite, positions_gauche)


def aspiration(dico_positions, positions_droite, positions_gauche):
    longBoucle = len(groupes_coureurs(dico_positions, positions_droite, positions_gauche)) # Calculer la longueur de la liste des groupes de coureurs
    listGroup = groupes_coureurs(dico_positions, positions_droite, positions_gauche) # Obtenir la liste des groupes de coureurs
    for i in range(longBoucle): # Parcourir chaque groupe de coureurs
        for groupe in listGroup[0:len(listGroup)-1]: # Parcourir tous les groupes sauf le dernier
            joueurCase = numero_de_case(groupe[-1], dico_positions) # Récupérer le numéro de case du dernier coureur dans le groupe
            if positions_droite[joueurCase + 1] == "__" and positions_droite[joueurCase + 2] != "__": # Vérifier si la case à droite du dernier coureur est vide et que la case après est occupée
                decale_coureurs(groupe, dico_positions, positions_droite, positions_gauche) # Si la condition est vérifiée, décaler les coureurs vers la droite

               

def tour_de_jeu(dico_positions, positions_droite, positions_gauche, dico_paquets, dico_defausses, configurations, affichage):
    tour_de_jeu_sans_effet(dico_positions, positions_droite, positions_gauche, dico_paquets, dico_defausses, configurations, affichage) #Premier étape : tour de jeu sans effet
    distribution_fatigue(dico_positions, positions_droite, positions_gauche) #Deuxième étape : distribution de la fatigue
    aspiration(dico_positions, positions_droite, positions_gauche) #Troisième étape : aspiration
