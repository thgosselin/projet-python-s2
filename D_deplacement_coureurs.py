from C_gestion_cartes import *
from B_initialisation_course import *

######### SECTION : DEPLACEMENT DES COUREURS #################
##############################################################

#Cette fonction détermine le nom du coureur qui est pour l'instant le plus loin
#du start dans la course.
#Elle prend en entrée le dictionnaire des positions
def le_plus_eloigne(dico_positions):
    max_case = -1
    epsilon = 0.5 #pour avantager les coureurs placés à droite
    max_joueur = ""
    for joueur in dico_positions:
        case = numero_de_case(joueur,dico_positions)
        if dico_positions[joueur][0] == "D":
            case += epsilon
        if case > max_case:
            max_joueur = joueur
            max_case = case
    return max_joueur

#Cette fonction détermine le nom du coureur qui est pour l'instant le plus
#en retrait dans la course.
#Elle prend en entrée le dictionnaire des positions et la liste des positions droite
def le_moins_eloigne(dico_positions,positions_droite):
    min_case = len(positions_droite)
    epsilon = 0.5 #pour avantager les coureurs placés à droite
    min_joueur = ""
    for joueur in dico_positions:
        case = numero_de_case(joueur,dico_positions)
        if dico_positions[joueur][0] == "D":
            case += epsilon
        if case < min_case:
            min_joueur = joueur
            min_case = case
    return min_joueur

#A chaque tour, le premier coureur à se déplacer est celui qui est en tête de
#la course, c'est-à-dire situé sur la plus haute case possible et à droite
#Cette fonction renvoie la liste des coureurs par ordre de placement sur la course
#Le coureur le plus proche de l'arrivée est situé en premier
def tri_joueurs(dico_positions):
    copie_dico = dict(dico_positions)
    ordre_joueurs = []
    while len(copie_dico) > 0:
        prochain_joueur = le_plus_eloigne(copie_dico)
        ordre_joueurs.append(prochain_joueur)
        del copie_dico[prochain_joueur]
    return ordre_joueurs


def avancee_coureurs(nom_joueur, dico_positions, positions_droite, positions_gauche, distance):
    # Recherche de l'indice du coureur dans les listes des positions droite et gauche
    indice_droite = -1  # Initialisation de l'indice du coureur dans la liste positions_droite
    indice_gauche = -1  # Initialisation de l'indice du coureur dans la liste positions_gauche
    # Recherche de l'indice dans la liste positions_droite
    for i, joueur in enumerate(positions_droite):
        if joueur == nom_joueur:  # Si le joueur est trouvé
            indice_droite = i  # On enregistre son indice
            break
    # Recherche de l'indice dans la liste positions_gauche
    for i, joueur in enumerate(positions_gauche):
        if joueur == nom_joueur:  # Si le joueur est trouvé
            indice_gauche = i  # On enregistre son indice
            break
    # Détermination de la nouvelle position possible du coureur
    nouvel_indice = -1  # Initialisation de la nouvelle position possible
    # Parcours des cases disponibles devant le coureur dans la liste positions_droite
    for i in range(indice_droite + 1, min(indice_droite + distance + 1, len(positions_droite))):
        if positions_droite[i] == "__":  # Si la case est vide
            nouvel_indice = i  # On enregistre la nouvelle position
            break
    # Si aucune position n'a été trouvée dans la liste positions_droite
    if nouvel_indice == -1:
        # Parcours des cases disponibles devant le coureur dans la liste positions_gauche
        for i in range(indice_gauche + 1, min(indice_gauche + distance + 1, len(positions_gauche))):
            if positions_gauche[i] == "__":  # Si la case est vide
                nouvel_indice = i  # On enregistre la nouvelle position
                break
    # Mise à jour des positions si une nouvelle position a été trouvée
    if nouvel_indice != -1:
        # Suppression du coureur de son ancienne position
        if indice_droite != -1:  # Si le coureur était sur la droite
            positions_droite[indice_droite] = "__"  # On le retire de la liste positions_droite
        if indice_gauche != -1:  # Si le coureur était sur la gauche
            positions_gauche[indice_gauche] = "__"  # On le retire de la liste positions_gauche
        # Placement du coureur dans sa nouvelle position
        if nouvel_indice > indice_droite:  # Si le nouvel indice est plus grand que l'ancien (déplacement vers la droite)
            positions_droite[nouvel_indice] = nom_joueur  # On place le coureur dans la nouvelle position droite
        else:  # Sinon (déplacement vers la gauche)
            positions_gauche[nouvel_indice] = nom_joueur  # On place le coureur dans la nouvelle position gauche 
        # Mise à jour du dictionnaire des positions du coureur
        dico_positions[nom_joueur] = "D" + str(nouvel_indice) if nouvel_indice > indice_droite else "G" + str(nouvel_indice)

def premiere_case_apres_arrivee(nb_lignes, taille_seg, position_terminus):
    nb_cases_toutes_lignes = (nb_lignes - 1) * taille_seg #Calcul du nombre total de cases sur toutes les lignes, sauf la dernière
    indice_premiere_case = nb_cases_toutes_lignes + position_terminus #Ajout du nombre de cases du dernier segment avant la ligne d'arrivée
    return indice_premiere_case

def test_victoire(nb_lignes, taille_seg, dico_positions, terminus):
    indice_premiere_case_apres_arrivee = premiere_case_apres_arrivee(nb_lignes, taille_seg, terminus) #Calcul de l'indice de la première case après la ligne d'arrivée
    for position in dico_positions.values(): #Parcours du dictionnaire des positions pour vérifier si un coureur a franchi la ligne d'arrivée
        numero_case = int(position[1:])
        if numero_case >= indice_premiere_case_apres_arrivee:
            return True
    return False



def tour_de_jeu_sans_effet(dico_positions, positions_droite, positions_gauche, dico_paquets, dico_defausses, configurations, affichage=False):
    if affichage:     # Vérifier si l'affichage est activé
        print("##### Début du tour #####")
    choix_joueurs = {} # Dictionnaire pour stocker les choix de déplacement de chaque joueur
    ordre_joueurs = tri_joueurs(dico_positions) # Obtenir l'ordre des joueurs en fonction de leur position sur la piste
    for joueur in ordre_joueurs: # Boucle à travers chaque joueur dans l'ordre défini
        choix = choix_deplacement(joueur, dico_paquets, dico_defausses, configurations, affichage) # Obtenir le choix de déplacement du joueur
        print(f"Le joueur {joueur} se déplace de {choix}") # Afficher le choix de déplacement du joueur
        choix_joueurs[joueur] = choix # Stocker le choix de déplacement dans le dictionnaire des choix de joueurs
    for joueur in ordre_joueurs: # Déplacer chaque joueur selon son choix de déplacement
        avancee_courreurs(joueur, dico_positions, positions_droite, positions_gauche, choix_joueurs[joueur])
    if affichage == True: # Si l'affichage est activé, afficher la fin du tour
        print("##### Fin du tour #####")


    

