from A_affichage_str import *
from E_phase_terminale import *
import tkinter as tk

#Affichage de l'entete du jeu dans la console
#Les arguments optionnels peuvent être modifiés pour
#personnaliser son étape !
def entete(numero_etape = 12, depart = "Aurillac", arrivee = "Villeneuve-sur-Lot"):
    chaine = "#" * 100 + "\n"
    chaine += "#"*41 + " LA FLAMME ROUGE " + "#"*41 + "\n"
    chaine += "#" * 100 + "\n" + "\n"
    chaine += "Bienvenue sur l'étape " + str(numero_etape) + " "
    chaine += "du Tour de France 2024 reliant " + depart + " "
    chaine += "à " + arrivee + " ! Bonne chance !!!"
    chaine += "\n" + "\n"
    return chaine

#Affichage de la route dans une fenêtre graphique
def afficher_fenetre(chaine, taille_fen = '600x300+50+10'):
    fen = tk.Tk()
    fen.geometry(taille_fen)
    fen.title("La Flamme Rouge")
    bouton = tk.Button(fen, text="Tour suivant", command=fen.destroy)
    tk.Label(fen, text=chaine).pack()
    bouton.pack(side=tk.LEFT, padx=5, pady=5)
    fen.mainloop()


def partie_jeu(nb_joueurs, nb_lignes, taille_seg, start=4, terminus=10, affichage=False):
    # Affichage de l'entête du jeu
    print(entete())
    
    # Initialisation des joueurs et des positions
    liste_joueurs = creer_liste_joueurs(nb_joueurs)
    positions_droite, positions_gauche = creer_listes_positions(nb_lignes, taille_seg)
    configurations = configurations_joueurs(liste_joueurs)
    dico_positions = construit_dico_positions(positions_droite, positions_gauche, start)

    # Initialisation des paquets
    dico_paquets = dico_paquets_initial(liste_joueurs)
    dico_defausses = dico_defausses_initial(liste_joueurs)

    # Initialisation de la ligne d'arrivée
    ligne_arrivee = nb_lignes - 1

    # Positionner les coureurs avant la ligne d'arrivée
    initialiser_jeu_alea(liste_joueurs, positions_droite, positions_gauche, start)

    if affichage:
        route = affichage_route(nb_lignes, taille_seg, positions_droite, positions_gauche, start, terminus)
        afficher_fenetre(route)

    # Tant que la ligne d'arrivée n'est pas atteinte
    while not test_victoire(nb_lignes, taille_seg, dico_positions, terminus):
        # Effectuer un tour de jeu
        tour_de_jeu(dico_positions, positions_droite, positions_gauche, dico_paquets, dico_defausses, configurations, affichage)

    # Afficher le nom du vainqueur
    vainqueur = determiner_vainqueur(dico_positions, positions_droite)
    print("Le vainqueur de l'étape est", vainqueur, "!!!")



        
