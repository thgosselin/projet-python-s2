from A_affichage_str import *
from B_initialisation_course import *
from C_gestion_cartes import *
from D_deplacement_coureurs import *
from E_phase_terminale import *
from F_jeu_complet import *


import tkinter as tk

def lancer_partie():
    nb_joueurs = int(entree_nb_joueurs.get())
    nb_lignes = int(entree_nb_lignes.get())
    taille_seg = int(entree_taille_segment.get())
    start = int(entree_start.get())
    terminus = int(entree_terminus.get())
    fenetre.destroy()
    partie_jeu(nb_joueurs, nb_lignes, taille_seg, start, terminus, affichage=True)


# Création de la fenêtre principale
fenetre = tk.Tk()
fenetre.title("Menu de La Flamme Rouge")

# Frame pour le choix du nombre de joueurs
frame_nb_joueurs = tk.Frame(fenetre)
frame_nb_joueurs.pack(pady=10)
label_nb_joueurs = tk.Label(frame_nb_joueurs, text="Nombre de joueurs :")
label_nb_joueurs.pack(side=tk.LEFT)
entree_nb_joueurs = tk.Entry(frame_nb_joueurs)
entree_nb_joueurs.pack(side=tk.LEFT)

# Frame pour le choix du nombre de segments
frame_nb_lignes = tk.Frame(fenetre)
frame_nb_lignes.pack(pady=10)
label_nb_lignes = tk.Label(frame_nb_lignes, text="Nombre de lignes :")
label_nb_lignes.pack(side=tk.LEFT)
entree_nb_lignes = tk.Entry(frame_nb_lignes)
entree_nb_lignes.pack(side=tk.LEFT)

# Frame pour le choix de la taille d'un segment
frame_taille_segment = tk.Frame(fenetre)
frame_taille_segment.pack(pady=10)
label_taille_segment = tk.Label(frame_taille_segment, text="Taille d'un segment :")
label_taille_segment.pack(side=tk.LEFT)
entree_taille_segment = tk.Entry(frame_taille_segment)
entree_taille_segment.pack(side=tk.LEFT)

# Frame pour le choix de la position de départ
frame_start = tk.Frame(fenetre)
frame_start.pack(pady=10)
label_start = tk.Label(frame_start, text="Position de départ (4 par défaut) :")
label_start.pack(side=tk.LEFT)
entree_start = tk.Entry(frame_start)
entree_start.pack(side=tk.LEFT)

# Frame pour le choix de la position de l'arrivée
frame_terminus = tk.Frame(fenetre)
frame_terminus.pack(pady=10)
label_terminus = tk.Label(frame_terminus, text="Position de l'arrivée (10 par défaut) :")
label_terminus.pack(side=tk.LEFT)
entree_terminus = tk.Entry(frame_terminus)
entree_terminus.pack(side=tk.LEFT)

# Frame pour le bouton de lancement de la partie
frame_lancer_partie = tk.Frame(fenetre)
frame_lancer_partie.pack(pady=10)
bouton_lancer_partie = tk.Button(frame_lancer_partie, text="Lancer la partie", command=lancer_partie)
bouton_lancer_partie.pack()






