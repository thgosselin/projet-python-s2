from A_affichage_str import *
from B_initialisation_course import *
from C_gestion_cartes import *
from D_deplacement_coureurs import *
from E_phase_terminale import *
from F_jeu_complet import *


import tkinter as tk


# Fonction pour afficher les joueurs sur la piste dans une fenêtre Tkinter
def afficher_piste(nb_joueurs, nb_lignes, taille_seg, positions_droite, positions_gauche, start=4):
    # Création de la fenêtre principale
    fenetre = tk.Tk()
    fenetre.title("La Flamme Rouge - Joueurs sur la piste")
    canvas = tk.Canvas(fenetre, width=800, height=400, bg="white") # Création du canvas pour dessiner la piste
    canvas.pack()
    dessiner_piste(canvas, nb_lignes, taille_seg, positions_droite, positions_gauche, start) # Dessiner la piste
    dessiner_joueurs(canvas, nb_joueurs, nb_lignes, taille_seg, positions_droite, positions_gauche, start) # Dessiner les joueurs
    bouton = tk.Button(fenetre, text="Tour suivant", command=fenetre.destroy)
    bouton.pack(side=tk.LEFT, padx=5, pady=5)
    fenetre.mainloop() # Lancement de la boucle principale Tkinter

# Fonction pour dessiner la piste
def dessiner_piste(canvas, nb_lignes, taille_seg, positions_droite, positions_gauche, start):
    # Dessiner la route
    ajout = 50
    canvas.create_rectangle(50, 50, 750, 100, fill="gray")
    for i in range(nb_lignes-1):
        canvas.create_rectangle(50, 50, 750, 100+ajout, fill="gray")
        ajout=ajout+50
    # Dessiner les lignes blanches sur la route
    ajout_ligne=50
    canvas.create_line(50, 100, 750, 100, fill="white", dash=(4, 2))
    for n in range(nb_lignes-2):
        canvas.create_line(50, 100+ajout_ligne, 750, 100+ajout_ligne, fill="white", dash=(4, 2))
        ajout_ligne=ajout_ligne+50
    dessiner_details_piste(canvas,nb_lignes) # Dessiner les détails de la piste (arbres, obstacles, etc.)

# Fonction pour dessiner les détails de la piste
def dessiner_details_piste(canvas, nb_lignes):
    # Dessiner des arbres sur le bord de la piste
    for i in range(100, 351, 100):
        canvas.create_rectangle(20, i - 10, 40, i + 10, fill="green")
        canvas.create_rectangle(760, i - 10, 780, i + 10, fill="green")

# Fonction pour dessiner les joueurs sur la piste
def dessiner_joueurs(canvas, nb_joueurs, nb_lignes, taille_seg, positions_droite, positions_gauche, start):
    # Positions de départ des joueurs
    x_start = 60
    y_start = 70
    for i in range(nb_joueurs): # Dessiner les joueurs à leurs positions de départ
        x = x_start + (i * 40)  # Espacement horizontal entre les joueurs
        y = y_start
        canvas.create_oval(x, y, x + 20, y + 20, fill="blue" if i % 2 == 0 else "red")  # Couleur différente pour chaque joueur


def partie_jeu(nb_joueurs, nb_lignes, taille_seg, start=4, terminus=10, affichage=False):
    print(entete())
    liste_joueurs = creer_liste_joueurs(nb_joueurs)
    positions_droite, positions_gauche = creer_listes_positions(nb_lignes, taille_seg)
    configurations = configurations_joueurs(liste_joueurs)
    dico_positions = construit_dico_positions(positions_droite, positions_gauche, start)
    dico_paquets = dico_paquets_initial(liste_joueurs)
    dico_defausses = dico_defausses_initial(liste_joueurs)
    ligne_arrivee = nb_lignes - 1
    initialiser_jeu_alea(liste_joueurs, positions_droite, positions_gauche, start)
    if affichage:
        afficher_piste(nb_joueurs, nb_lignes, taille_seg, positions_droite, positions_gauche, start)
    while not test_victoire(nb_lignes, taille_seg, dico_positions, terminus):
        tour_de_jeu(dico_positions, positions_droite, positions_gauche, dico_paquets, dico_defausses, configurations, affichage)
    vainqueur = determiner_vainqueur(dico_positions, positions_droite)
    print("Le vainqueur de l'étape est", vainqueur, "!!!")

partie_jeu(6,5,20,affichage = True)

