import pygame
from A_affichage_str import *
from B_initialisation_course import *
from C_gestion_cartes import *
from D_deplacement_coureurs import *
from E_phase_terminale import *
from F_jeu_complet import *
import tkinter as tk

pygame.init()# Initialisation de pygame 

chemin_musique = "Musique\musique.mp3" # Chemin vers le fichier audio

# Fonction pour jouer de la musique en arrière-plan
def jouer_musique():
    pygame.mixer.music.load(chemin_musique)
    pygame.mixer.music.play(-1)  # -1 signifie de jouer en boucle

# Fonction pour arrêter la musique
def arreter_musique():
    pygame.mixer.music.stop()


def partie_jeu(nb_joueurs, nb_lignes, taille_seg, start=4, terminus=10, affichage=False):
    # Affichage de l'entête du jeu
    print(entete())
    # Jouer de la musique en arrière-plan
    jouer_musique()
    
    # Initialisation des joueurs et des positions
    liste_joueurs = creer_liste_joueurs(nb_joueurs)
    positions_droite, positions_gauche = creer_listes_positions(nb_lignes, taille_seg)
    configurations = configurations_joueurs(liste_joueurs)
    dico_positions = construit_dico_positions(positions_droite, positions_gauche, start)

    # Initialisation des paquets
    dico_paquets = dico_paquets_initial(liste_joueurs)
    dico_defausses = dico_defausses_initial(liste_joueurs)

    # Initialisation de la ligne d'arrivée
    ligne_arrivee = nb_lignes - 1

    # Positionner les coureurs avant la ligne d'arrivée
    initialiser_jeu_alea(liste_joueurs, positions_droite, positions_gauche, start)

    if affichage:
        route = affichage_route(nb_lignes, taille_seg, positions_droite, positions_gauche, start, terminus)
        afficher_fenetre(route)

    # Tant que la ligne d'arrivée n'est pas atteinte
    while not test_victoire(nb_lignes, taille_seg, dico_positions, terminus):
        # Effectuer un tour de jeu
        tour_de_jeu(dico_positions, positions_droite, positions_gauche, dico_paquets, dico_defausses, configurations, affichage)

    # Arrêter la musique
    arreter_musique()

    # Afficher le nom du vainqueur
    vainqueur = determiner_vainqueur(dico_positions, positions_droite)
    print("Le vainqueur de l'étape est", vainqueur, "!!!")

# Exemple d'utilisation
partie_jeu(6,5,20,affichage = True)

